package algorithm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

public class PartOfSpeechCalculatorService 
{
	public Map<String, String> wordToMostLikelyTag;
	public Map<String, Double> wordToProbability;
	
	public PartOfSpeechCalculatorService()
	{
		wordToMostLikelyTag = new HashMap<>();
		wordToProbability = new HashMap<>();
	}
	
	public void calculateMaxProbability(String contents, ArrayList<String> words, ArrayList<String> tags)
	{
		for(int index = 0; index < words.size(); index++)
		{
			String word = words.get(index);
			String tag = tags.get(index);
			String tagBefore;
			
			if(index == 0)
			{
				tagBefore = "";	
			}
			else
			{
				tagBefore = tags.get(index - 1);
			}
			
			double likelihood = getLikelihood(contents, word, tag);
			double prior = getPrior(contents, word, tag, tagBefore, tags);
			
			Double probability = likelihood * prior;
			
			if(wordToProbability.containsKey(word))
			{
				double currentProbability = wordToProbability.get(word);
				
				if(probability > currentProbability)
				{
					wordToProbability.put(word, currentProbability);
					wordToMostLikelyTag.put(word, tag);
				}
			}
			else
			{
				wordToProbability.put(word, probability);
				wordToMostLikelyTag.put(word, tag);
			}
		}		
	}

	private double getLikelihood(String contents, String word, String tag) 
	{
		int countOfTagToWord =  StringUtils.countMatches(contents, word + "/" + tag);
		int countOfTag =  StringUtils.countMatches(contents, tag);

		return countOfTagToWord / (countOfTag * 1.0);
	}

	private double getPrior(String contents, String word, String tag, String tagBefore, ArrayList<String> tags) 
	{
		String tagsInOrder = tags.toString();
		
		int countOfPreviousTag = StringUtils.countMatches(contents, tagBefore);
		int countofPreviousTagThenCurrentTag = StringUtils.countMatches(contents, tagBefore + ", " + tag);
		
		return countofPreviousTagThenCurrentTag / (countOfPreviousTag * 1.0);
	}

	public String getTagForToken(String token) 
	{
		String tag =  wordToMostLikelyTag.get(token);
		
		if(tag == null)
		{
			tag = getUnknownTag();
		}

		if(tokenIsPunctuation(token))
		{
			tag = handlePunctuationforToken(token);
		}
		
		return tag;
	}
	
	private String getUnknownTag() 
	{
		return "NN";
	}

	private String handlePunctuationforToken(String token) 
	{
		return token;
	}
	
	private boolean tokenIsPunctuation(String token) 
	{
		return Pattern.matches("\\p{Punct}", token);
	}

	public void printErrorMatrixToConsole(String goldenStandardContents, String predictionContents, Tokenizer tokenizer) 
	{
		tokenizer.tokenizeTrainingData(goldenStandardContents);
		ArrayList<String> goldenStandardTags = tokenizer.tags;
		
		for(int j=0;j< goldenStandardTags.size(); j++)
		{
			String tag = goldenStandardTags.get(j);
			
			if(tokenIsPunctuation(tag) || tag.contains("."))
			{
				goldenStandardTags.remove(tag);
			}
		}
		
		tokenizer.tokenizeTrainingData(predictionContents);
		ArrayList<String> predictionTags = tokenizer.tags;
		
		for(int j=0;j< predictionTags.size(); j++)
		{
			String tag = predictionTags.get(j);
			
			if(tokenIsPunctuation(tag) || tag.contains("."))
			{
				predictionTags.remove(tag);
			}
		}
		
			System.out.println(goldenStandardTags);
			System.out.println(predictionTags);	
			System.out.println(goldenStandardTags.size());
			System.out.println(predictionTags.size());
	
	}
}
