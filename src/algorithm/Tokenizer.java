package algorithm;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Pattern;

import org.apache.commons.lang.ArrayUtils;

public class Tokenizer 
{
	public ArrayList<String> tokens;
	public ArrayList<String> words;
	public ArrayList<String> tags;
	
	public Tokenizer()
	{}
	
	public void tokenizeTrainingData(String fileContents)
	{
		tokens = new ArrayList<>(Arrays.asList(fileContents.split(" ")));
		
		words = new ArrayList<>();
		tags = new ArrayList<>();
		
		for(String token : tokens)
		{
			String [] wordTagPair = token.split("/");
			
			words.add(wordTagPair[0]);
			tags.add(wordTagPair[1]);
		}
	}

	public String [] getTokens(String testDataContents) 
	{
		String [] tokens = testDataContents.split("\\b");
	
		for(String token : tokens)
		{
			if(Pattern.matches("[ \\t]+", token))
			{
				tokens = (String [])ArrayUtils.removeElement(tokens, token);
			}
		}
		return tokens;
	}
}
