package services;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Map;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import algorithm.PartOfSpeechCalculatorService;

public class FileService 
{
	public FileService()
	{}
	
	public String readTextFile(String fileName) throws FileNotFoundException
	{
		File file = new File(fileName + ".txt");
	    
		Scanner sc = new Scanner(file);
		String contents = ""; 
	  
		while (sc.hasNextLine())
	    {
			if(contents.length() == 0)
			{
		    	contents += sc.nextLine();
			}
			else
			{
				contents += "\n" + sc.nextLine();
			}
		}
		
		return contents;
	}

	public void writeResultToFile(String[] tokens, PartOfSpeechCalculatorService service, String fileName) throws FileNotFoundException, UnsupportedEncodingException 
	{
		String originalFileWithTags = readTextFile("inputData/" + fileName);
		String regexMatch = "";
		String regexReplace = "";
		String output = "";
		
		for(int i = 0;i < tokens.length; i++)
		{
			String token = tokens[i];
			
			String tag = service.getTagForToken(token);

			regexMatch += "(.*" + token + ")";
			regexReplace += "$" + (i + 1) + "/" + tag; 
		}

		Pattern p = Pattern.compile(regexMatch);
		
		Matcher m = p.matcher(originalFileWithTags);
		if (m.find()) 
		{
		    output = m.replaceFirst(regexReplace);
		}
		
		PrintWriter writer = new PrintWriter("outputData/" + fileName + ".txt", "UTF-8");
		writer.println(output);
		writer.close();
	}
}
