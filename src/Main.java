import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;

import javax.swing.JOptionPane;

import algorithm.PartOfSpeechCalculatorService;
import algorithm.Tokenizer;
import services.FileService;

public class Main 
{
	FileService fileReader;
	Tokenizer tokenizer;
	PartOfSpeechCalculatorService service;
	
	public Main()
	{
		fileReader = new FileService();
		tokenizer = new Tokenizer();
		service = new PartOfSpeechCalculatorService();
	}
	
	public void startUp() throws FileNotFoundException, UnsupportedEncodingException
	{
		String trainingDataContents = fileReader.readTextFile("trainingData/pos_tagged");
		tokenizer.tokenizeTrainingData(trainingDataContents);
		service.calculateMaxProbability(trainingDataContents, tokenizer.words, tokenizer.tags);
		
		String fileName = JOptionPane.showInputDialog("Please input the name of a file: ");
				
		String testDataContents = fileReader.readTextFile("inputData/" + fileName);
		String [] tokens = tokenizer.getTokens(testDataContents);

		fileReader.writeResultToFile(tokens, service, fileName);		
	
		int dialogResult = JOptionPane.showConfirmDialog (null, "Would you like to input a golden standard file and generate a confusion matrix?", "Info", 0);
		
		if (dialogResult == JOptionPane.YES_OPTION)
		{
			String fileNameGS = JOptionPane.showInputDialog("Please input the name of the golden standard file: ");
			
			String goldenStandardContents = fileReader.readTextFile("goldenStandard/" + fileNameGS);
			String predictionContents = fileReader.readTextFile("outputData/" + fileName);
			
			service.printErrorMatrixToConsole(goldenStandardContents, predictionContents, tokenizer);
		}
	}
	
	public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException
	{
		new Main().startUp();
	}
}
