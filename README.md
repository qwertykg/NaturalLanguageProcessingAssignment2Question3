# Natural Language Processing Assignment 2 Question 3
The programming portion of Assignment 2 of UNISA's Honors level Natual Language Processing course
## Installation
Run "git clone git@gitlab.com:qwertykg/NaturalLanguageProcessingAssignment2Question3.git"
Open project in eclipse and compile and run project
## Important things to consider
The end result is written to the location "./outputData/inputFileName" where "inputFileName" is the name of the file you inputed. Note that if the file exists in "./outputData" the contents will be overwritten. Otherwise the file will be generated, in eclipse in order to recognize this change you might have to refresh the project for eclipse to show you the file in the project structure. Otherwise you can simply view the file in your file explorer.